package main

import (
	"context"
	"log"
	"os"
	"syscall"
	"time"

	"gitlab.com/joewreschnig/go-signalcontext"
)

func main() {
	log.Print("waiting 10 seconds or until signal...")
	p, cancel := context.WithTimeout(context.Background(), time.Second*10)
	ctx := signalcontext.UntilSignal(p, os.Interrupt, syscall.SIGTERM)
	<-ctx.Done()
	cancel()
	log.Print(ctx.Err())
}
