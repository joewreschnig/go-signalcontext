package signalcontext

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestError(t *testing.T) {
	assert.EqualError(t, Error{os.Interrupt},
		"received signal: "+os.Interrupt.String())
}

func BenchmarkError(b *testing.B) {
	expected := "received signal: " + os.Interrupt.String()
	for i := 0; i < b.N; i++ {
		if s := (Error{os.Interrupt}).String(); s != expected {
			b.Fatalf("expected %s, got %s", s, expected)
		}
	}
}
