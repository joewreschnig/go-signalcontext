package signalcontext

import (
	"os"
)

// A Error will be returned by a SignalContext’s Err() method when it
// canceled due to an operating system signal (rather than e.g. parent
// cancellation).
type Error struct {
	os.Signal
}

func (e Error) Error() string {
	return e.String()
}

func (e Error) String() string {
	return "received signal: " + e.Signal.String()
}
